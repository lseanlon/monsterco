/*read file and return list array*/
function ReadLines(_input, _scb) {
    var remaining = '';
    var listArrayLine=[];
    _input.on('data', function(data) {
        remaining += data;
        var index = remaining.indexOf('\n');
        while (index > -1) {
            var line = remaining.substring(0, index);
            remaining = remaining.substring(index + 1);
            
            listArrayLine.push(line);
            index = remaining.indexOf('\n');
        }
    });

    _input.on('end', function() {
        if (remaining.length > 0) {
        	// console.log('remaining', remaining);

            listArrayLine.push(remaining);
            _scb(listArrayLine);
        }
    });
}




module.exports = ReadLines;
