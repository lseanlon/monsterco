var cheerio = require('cheerio');
var fs = require('fs');

var download = require('download');
var categoryListString = fs.readFileSync('./dist/data-categorylist.txt', 'utf8');
var categoryList = JSON.parse(categoryListString)
var listRowPage = [];
categoryList.forEach(function(_item) {
    //     console.log(_item);
    var localpath = "./dist/category/" + _item.url.replace('http://123movies.is/genre/', '');
    var htmlcategory = fs.readFileSync(localpath, 'utf8');
    var outputpath = "./dist/category" + _item.title + "/"

    var $ = cheerio.load(htmlcategory)

    //find the last index page 
    var lastIndexPage = 1;
    if ($("a:contains('Last >>')").eq(1)) {
        lastIndexPage = $("a:contains('Last >>')").eq(1).prop('data-ci-pagination-page');;

        lastIndexPage = lastIndexPage ? lastIndexPage : 1;
        // console.log('lastIndexPage',lastIndexPage);
    }
    lastIndexPage = parseInt(lastIndexPage);
    var row = { "path": outputpath, "lastIndexPage": lastIndexPage };
    listRowPage.push(row);


    // loop and download from first page to last index page.
    for (var idx = 1; idx <= lastIndexPage; idx++) {
        var indexUrl = _item.url + "" + idx;

        console.log('start download - ', indexUrl);
        download(indexUrl, outputpath).then(() => {
            console.log('done download - ', indexUrl);
        });
    }



});


var savePath = "./dist/data-category-page.txt";
fs.appendFile(savePath, JSON.stringify(listRowPage), function(err) {
    if (err) {
        return console.log(err);
    }

    console.log("The file was saved!", savePath);
});
